FROM ubuntu:18.04
RUN apt update -y && apt install software-properties-common -y
RUN apt-add-repository --yes --update ppa:ansible/ansible 
RUN apt update -y && apt install ansible git -y
ADD ansible-update.sh /root/
ENTRYPOINT ["/bin/bash", "/root/ansible-update.sh"]
